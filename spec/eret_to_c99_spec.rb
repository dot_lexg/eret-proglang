# frozen_string_literal: true

require 'yaml'
require 'eret_to_c99'

RSpec.describe EretToC99 do
  Dir[File.expand_path('testcases/*.yml', __dir__)].each do |filename|
    it "parses #{File.basename(filename)} as expected" do
      testcase = YAML.safe_load(File.read(filename), permitted_classes: TESTCASE_TYPES, aliases: true)
      expect(described_class.translate(testcase[:ast])).to eq(testcase[:translated_ast])
    end
  end
end
