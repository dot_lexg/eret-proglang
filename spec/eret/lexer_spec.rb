# frozen_string_literal: true

require 'yaml'
require 'eret'

RSpec.describe Eret::Lexer do
  Dir[File.expand_path('../testcases/*.yml', __dir__)].each do |filename|
    it "parses #{File.basename(filename)} as expected" do
      testcase = YAML.safe_load(File.read(filename), permitted_classes: TESTCASE_TYPES, aliases: true)
      expect(described_class.lex(testcase[:source]).to_a).to eq(testcase[:tokens])
    end
  end
end
