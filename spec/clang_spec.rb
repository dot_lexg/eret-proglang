# frozen_string_literal: true

require 'yaml'

RSpec.describe 'clang -std=c99 compiles' do
  Dir[File.expand_path('testcases/*.yml', __dir__)].each do |filename|
    context File.basename(filename) do
      let(:testcase) { YAML.safe_load(File.read(filename), permitted_classes: TESTCASE_TYPES, aliases: true) }

      it "compiles #{File.basename(filename)} without warnings/errors" do
        IO.popen(%w[clang -std=c99 -xc -o /dev/null -], 'r+', err: [:child, :out]) do |io|
          io.puts(testcase[:translated_ast].header.to_s)
          io.puts(testcase[:translated_ast].implementation.to_s)
          io.close_write
          expect(io.read).to be_empty
        end
        expect(Process.last_status).to be_success
      end
    end
  end
end
