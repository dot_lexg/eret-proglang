# frozen_string_literal: true

require 'eret'
require 'c99'

TESTCASE_TYPES = [
  Symbol,
  *Eret::AST.constants.map(&Eret::AST.method(:const_get)),
  *C99::AST.constants.map(&C99::AST.method(:const_get))
].freeze

# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true # default in RSpec 4
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true # default in RSpec 4
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups # default in rspec4
  config.filter_run_when_matching :focus
  config.example_status_persistence_file_path = 'spec/examples.txt'
  config.disable_monkey_patching!
  config.warnings = true
  config.default_formatter = 'doc' if config.files_to_run.one?
  # config.profile_examples = 3

  config.order = :random
  Kernel.srand config.seed
end
