# frozen_string_literal: true

require 'strscan'

module Eret
  module Lexer
    TOKENS = [
      [:NL, /(?:(#.*)?\r?\n)+/],
      [:ignore, /\s+/],
      [nil, /const|def|end|if|let|require|return/], # keywords
      [nil, /::|>=|==|!=|<=/], # multi-char operators
      [nil, %r{[!%&()*+,\-./:;<=>?\[\]^|~]}], # single char operators
      [:ID, /[a-zA-Z_]\w*/],
      [:STR_LITERAL, /"(?:\\.|[^\\"])*"/],
      [:INT_LITERAL, /0x[\d_a-fA-F]+|0o?[0-7_]+|[1-9][\d_]*|0/],
    ].freeze

    def self.lex(str)
      return enum_for(:lex, str) if !block_given?

      scanner = StringScanner.new(str)
      until scanner.eos?
        token, value = lex_once(scanner)
        yield [token, value] unless token == :ignore
      end
    end

    def self.lex_once(scanner)
      TOKENS.each do |(token, regex)|
        value = scanner.scan(regex)
        return [token || value, value] if value
      end

      raise "Unexpected character: #{scanner.peek(1).inspect} in #{scanner.inspect}"
    end
  end
end
