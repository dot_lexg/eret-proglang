# frozen_string_literal: true

require_relative '../eret_to_c99'

module Eret
  module Compiler
    def self.puts_token_list(token_list)
      token_list.each do |token, value|
        puts "#{token.to_s.ljust(16)} #{value.inspect}"
      end
      puts
    end

    def self.puts_ast(ast)
      pp ast
      ast.to_s.each_line do |line|
        puts line
      end
      puts
    end

    def self.compile(source_code, header_filename:, debug: false)
      eret_ast = begin
        Eret.parse(Eret.lex(source_code))
      rescue StandardError
        puts_token_list token_list if debug
        raise
      end
      puts_ast eret_ast if debug

      c99_ast = EretToC99.translate(eret_ast)
      puts_ast c99_ast if debug

      implementation = c99_ast.implementation.dup
      implementation.toplevels.unshift(C99::AST::LocalInclude.new(header_filename)) if header_filename
      {
        header: c99_ast.header.to_s,
        implementation: implementation.to_s
      }
    end
  end
end
