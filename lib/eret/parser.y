class Eret::Parser
token NL ID STR_LITERAL INT_LITERAL
prechigh
	nonassoc UMINUS '['
	left '*' '/'
	left '+' '-'
	left '<' '<=' '==' '!=' '>=' '>'
preclow
rule
	compilation_unit : toplevel_expressions _ { result = AST::CompilationUnit.new(result) }
	toplevel_expressions
		: toplevel_expressions toplevel_expression terminus { result << val[1] if val[1] }
		| toplevel_expression terminus { result = [result] }
		| terminus { result = [] }
	toplevel_expression
		: 'def' _ namespaced_id _ '(' _ params ')' _ ':' _ type terminus statements 'end' { result = AST::Function.new(val[2], val[6], val[11], val[13]) }
		| 'require' _ STR_LITERAL { result = AST::Require.new(val[2]) }

	params
		: params ',' _ param _ { result << val[3] if val[3] }
		| param _ { result = [result] }
		| { result = [] }
	param
		: ID ':' _ type { result = AST::Param.new(val[0], val[3]) }

	statements
		: statements statement terminus { result << val[1] if val[1] }
		| statement terminus { result = [result] }
		| terminus
	statement
		: expression { result = AST::Statement.new(val[0]) }
		| 'let' ID ':' _ type '=' _ expression { result = AST::LetEqualsStatement.new(val[1], val[4], val[7]) }
		| 'return' _ expression { result = AST::ReturnStatement.new(val[2]) }
		| 'if' expression terminus statements 'end' { result = AST::IfBlock.new(val[1], val[3]) }

	arguments
		: arguments ',' _ expression _ { result << val[3] if val[3] }
		| expression _ { result = [result] }
		| { result = [] }
	expression
		: expression '*' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '/' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '+' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '-' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '<' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '>' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '==' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '<=' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '!=' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '>=' _ expression { result = AST::Infix.new(val[0], val[1], val[3]) }
		| expression '[' _ expression ']' { result = AST::Subscript.new(val[0], val[3]) }
		| '-' expression =UMINUS { result = AST::Prefix.new('-', val[1]) }
		| '(' expression ')' { result = val[1] }
		| namespaced_id '(' _ arguments ')' { result = AST::Call.new(val[0], val[3]) }
		| ID { result = AST::Reference.new(result) }
		| STR_LITERAL { result = AST::StrLiteral.new(result) }
		| INT_LITERAL { result = AST::IntLiteral.new(result) }

	type
		: ID pointers { result = val.join(' ') }
		| 'const' ID pointers { result = val.join(' ') }
		| ID
		| 'const' ID { result = val.join(' ') }
	pointers
		: pointers '*' { result = val.join }
		| '*'
	namespaced_id
		: ID { result = AST::NamespacedId.new(nil, val[0]) }
		| ID '::' ID { result = AST::NamespacedId.new(val[0], val[2]) }

	terminus: NL | ';'
	_: | NL # Short for "optional newline" because this can be used in many places
end
