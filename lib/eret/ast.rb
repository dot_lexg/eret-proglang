# frozen_string_literal: true

require_relative '../ast_node'
require_relative '../c99'
require_relative '../lang_utils'

module Eret
  module AST
    CompilationUnit = ASTNode.new(:toplevels) do
      def to_s = toplevels.map {|toplevel| "#{toplevel}\n" }.join

      def to_c99_ast
        C99::AST::CompilationUnit.new(
          LangUtils.generate_c99_header(toplevels),
          C99::AST::File.new(toplevels.map(&:to_c99_ast))
        )
      end
    end

    Require = ASTNode.new(:required_spec) do
      def initialize(quoted_required_spec) = super(LangUtils.unquote_string(quoted_required_spec))
      def to_s = "require #{LangUtils.quote_string(required_spec)}"

      def to_c99_ast
        library, header = required_spec.split('/', 2)
        raise Eret::SyntaxError, 'Only libc requires are supported!' if library != 'libc'

        header += '.h' unless header =~ /\.h\z/
        C99::AST::LibraryInclude.new(header)
      end
    end

    Function = ASTNode.new(:namespaced_id, :params, :return_type, :statements) do
      def to_s
        "#{namespaced_id}(#{params.map(&:to_s).join(', ')}): #{return_type}\n#{LangUtils.indent(statements)}end"
      end

      def to_c99_ast
        C99::AST::Function.new(
          namespaced_id.mangle,
          params.map(&:to_c99_ast),
          return_type,
          statements.map(&:to_c99_ast)
        )
      end
    end

    NamespacedId = ASTNode.new(:namespace, :id) do
      def to_s = namespace ? "#{namespace}::#{id}" : id

      def mangle
        return id if namespace == 'libc'

        escaped = [namespace, id].compact.map do |x|
          x.gsub('_', '__')
        end.join('_q')
        "#{escaped.sub(/\A__/, '_x__')}_ERET"
      end
    end

    Param = ASTNode.new(:id, :type) do
      def to_s = "#{id}: #{type}"
      def to_c99_ast = C99::AST::Param.new(id, type)
    end

    Statement = ASTNode.new(:expression) do
      def to_s = expression.to_s
      def to_c99_ast = C99::AST::Statement.new(expression.to_c99_ast)
    end

    LetEqualsStatement = ASTNode.new(:id, :type, :value) do
      def to_s = "let #{id}: #{type} = #{value}"
      def to_c99_ast = C99::AST::LetEqualsStatement.new(id, type, value.to_c99_ast)
    end

    ReturnStatement = ASTNode.new(:value) do
      def to_s = "return #{value}"
      def to_c99_ast = C99::AST::ReturnStatement.new(value.to_c99_ast)
    end

    Prefix = ASTNode.new(:op, :rhs) do
      def to_s = "(#{op}#{rhs})"
      def to_c99_ast = C99::AST::Prefix.new(op, rhs.to_c99_ast)
    end

    Infix = ASTNode.new(:lhs, :op, :rhs) do
      def to_s = "(#{lhs} #{op} #{rhs})"
      def to_c99_ast = C99::AST::Infix.new(lhs.to_c99_ast, op, rhs.to_c99_ast)
    end

    Subscript = ASTNode.new(:array, :index) do
      def to_s = "#{array}[#{index}]"
      def to_c99_ast = C99::AST::Subscript.new(array.to_c99_ast, index.to_c99_ast)
    end

    Call = ASTNode.new(:namespaced_id, :params) do
      def to_s = "#{namespaced_id}(#{params.map(&:to_s).join(', ')})"
      def to_c99_ast = C99::AST::Call.new(namespaced_id.mangle, params.map(&:to_c99_ast))
    end

    Reference = ASTNode.new(:id) do
      def to_s = id
      def to_c99_ast = C99::AST::Reference.new(id)
    end

    StrLiteral = ASTNode.new(:value) do
      def initialize(quoted_str) = super(LangUtils.unquote_string(quoted_str))
      def to_s = LangUtils.quote_string(value)
      def to_c99_ast = C99::AST::StrLiteral.new(value)
    end

    IntLiteral = ASTNode.new(:value) do
      def initialize(int) = super(Integer(int))
      def to_s = value.to_s
      def to_c99_ast = C99::AST::IntLiteral.new(value)
    end

    IfBlock = ASTNode.new(:condition, :true_branch) do
      def to_s = "if #{condition}\n#{LangUtils.indent(true_branch)}end"
      def to_c99_ast = C99::AST::IfBlock.new(condition.to_c99_ast, true_branch.map(&:to_c99_ast))
    end
  end
end
