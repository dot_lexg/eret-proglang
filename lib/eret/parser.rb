# frozen_string_literal: true

require_relative '../../gen/eret/parser'

module Eret
  class Parser
    def self.parse(tokens)
      new(tokens).parse
    end

    def initialize(tokens)
      @token_enumerator = tokens.each
    end

    alias parse do_parse

    def next_token
      @token_enumerator.next
    rescue StopIteration
      [false, '$end']
    end
  end
end
