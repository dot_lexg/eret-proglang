# frozen_string_literal: true

module Eret
  module BuildSystem
    def self.sh(argv)
      puts argv.join(' ')
      system([argv[0], argv[0]], *argv[1..])
    end

    def self.build_from_manifest(base_path, executable, sources:)
      raise ArgumentError, 'sources must be an array of filenames' unless sources.is_a?(Array)

      build_path = File.join(base_path, 'build')
      eretc = File.expand_path('../../exe/eretc', __dir__)

      sh %W[rm -rf #{build_path}]

      object_filenames = sources.map do |source|
        source_filename = File.expand_path(source, base_path)
        c99_filename = File.expand_path("#{source}.c", build_path)
        object_filename = File.expand_path("#{source}.o", build_path)

        sh %W[mkdir -p #{File.dirname(c99_filename)}]
        sh %W[#{eretc} -o #{c99_filename} #{source_filename}]
        sh %W[cc -Wall -Werror -std=c99 -c -o #{object_filename} #{c99_filename}]
        object_filename
      end

      executable_filename = File.expand_path(executable, build_path)
      sh %W[cc -Wall -Werror -std=c99 -o #{executable_filename}] + object_filenames
    end
  end
end
