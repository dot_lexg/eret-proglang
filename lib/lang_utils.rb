# frozen_string_literal: true

require 'strscan'

module LangUtils
  def self.unquote_string(quoted_str)
    raise ArgumentError unless quoted_str.start_with?('"') && quoted_str.end_with?('"')

    str = String.new
    scanner = StringScanner.new(quoted_str[1...-1])
    until scanner.eos?
      if (chars = scanner.scan(/[^\\"]+/))
        str << chars
      elsif (escape_seq = scanner.scan(/\\./))
        str << unescape(escape_seq)
      else
        raise
      end
    end
    str
  end

  def self.quote_string(str)
    str.inspect # XXX: prints a ruby string not a eret string! (same thing in most cases *wink*)
  end

  def self.unescape(escape_seq)
    {
      '\\\\' => '\\',
      '\r' => "\r",
      '\t' => "\t",
      '\n' => "\n"
    }[escape_seq] || raise(Racc::ParseError, "Invalid escape sequence #{escape_seq}")
  end

  def self.indent(value, indent = '  ')
    value = value.map {|x| "#{x}\n" }.join if value.is_a?(Array)

    value.to_s.lines.map do |line|
      indent + line
    end.join
  end

  def self.c99_param_list(params)
    if params.empty?
      'void'
    else
      params.map(&:to_s).join(', ')
    end
  end

  # TODO: convert this into a node-level filter
  def self.generate_c99_header(toplevels)
    functions = toplevels.select do |toplevel|
      toplevel.is_a?(Eret::AST::Function)
    end
    C99::AST::File.new(functions.map do |function|
      c99_function = function.to_c99_ast
      C99::AST::FunctionPrototype.new(c99_function.id, c99_function.params, c99_function.return_type)
    end)
  end
end
