# frozen_string_literal: true

module EretToC99
  def self.translate(ast)
    [
      :to_c99_ast,
    ].reduce(ast) do |src_ast, msg|
      src_ast.public_send_recursive(msg)
    end
  end
end
