# frozen_string_literal: true

require_relative '../ast_node'
require_relative '../lang_utils'

module C99
  module AST
    CompilationUnit = ASTNode.new(:header, :implementation)

    File = ASTNode.new(:toplevels) do
      def to_s = toplevels.map {|toplevel| "#{toplevel}\n" }.join
    end

    LibraryInclude = ASTNode.new(:filename) do
      def to_s = "#include <#{filename}>"
    end

    LocalInclude = ASTNode.new(:filename) do
      def to_s = "#include #{LangUtils.quote_string(filename)}"
    end

    Function = ASTNode.new(:id, :params, :return_type, :statements) do
      def to_s = "#{return_type} #{id}(#{LangUtils.c99_param_list(params)}) {\n#{LangUtils.indent(statements)}}"
    end

    FunctionPrototype = ASTNode.new(:id, :params, :return_type) do
      def to_s = "#{return_type} #{id}(#{LangUtils.c99_param_list(params)});"
    end

    Param = ASTNode.new(:id, :type) do
      def to_s = "#{type} #{id}"
    end

    Statement = ASTNode.new(:expression) do
      def to_s = "#{expression};"
    end

    LetEqualsStatement = ASTNode.new(:id, :type, :value) do
      def to_s = "#{type} #{id} = #{value};"
    end

    ReturnStatement = ASTNode.new(:expression) do
      def to_s = "return #{expression};"
    end

    Prefix = ASTNode.new(:op, :rhs) do
      def to_s = "(#{op}#{rhs})"
    end

    Infix = ASTNode.new(:lhs, :op, :rhs) do
      def to_s = "(#{lhs} #{op} #{rhs})"
    end

    Subscript = ASTNode.new(:array, :index) do
      def to_s = "#{array}[#{index}]"
    end

    Call = ASTNode.new(:id, :params) do
      def to_s = "#{id}(#{params.map(&:to_s).join(', ')})"
    end

    Reference = ASTNode.new(:id) do
      def to_s = id
    end

    StrLiteral = ASTNode.new(:value) do
      def to_s = LangUtils.quote_string(value)
    end

    IntLiteral = ASTNode.new(:value) do
      def to_s = value.to_s
    end

    IfBlock = ASTNode.new(:condition, :true_branch) do
      def to_s = "if (#{condition}) {\n#{LangUtils.indent(true_branch)}}"
    end
  end
end
