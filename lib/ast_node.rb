# frozen_string_literal: true

module ASTNode
  def self.new(*members, &block)
    struct = Struct.new(*members, &block)
    struct.include(self)
    struct
  end

  def public_send_recursive(msg, *args, **kwargs, &block)
    if respond_to?(msg)
      public_send(msg, *args, **kwargs, &block)
    else
      filtered_values = values.map do |value|
        case value
        when Array
          value.map do |element|
            element.public_send_recursive(msg, *args, **kwargs, &block)
          end
        when ASTNode
          value.public_send_recursive(msg, *args, **kwargs, &block)
        else
          value
        end
      end
      self.class.new(filtered_values)
    end
  end
end
