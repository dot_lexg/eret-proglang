# frozen_string_literal: true

require 'forwardable'

require_relative 'eret/ast'
require_relative 'eret/build_system'
require_relative 'eret/compiler'
require_relative 'eret/lexer'
require_relative 'eret/parser'

module Eret
  class << self
    extend Forwardable
    def_delegator Eret::BuildSystem, :build_from_manifest
    def_delegator Eret::Compiler, :compile
    def_delegator Eret::Lexer, :lex
    def_delegator Eret::Parser, :parse
  end
end
