# Roadmap

Also it's probably a good idea to either come up with a third package for
"synthetic" nodes to provide an intermediate state for filtering.
However, it is pointless to create mirror intermediate node classes if the
filter is able to return a C99 construct.
The idea is that the Eret and C99 ASTs define `#to_s` to return equivalent code,
and represent a semi-stable interface for each language.
The synthetic nodes can be modified as needed to support whatever language
features are needed in a potentially decentralized way, without affecting the
API of the two stable ASTs.

Pull out identifier mangling into its own class and make it more general.
